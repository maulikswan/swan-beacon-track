from bluetooth.ble import BeaconService
import requests
import schedule
import time
import datetime
import logging
import json

import sqlite3 as lite
import sys
import database_service as db_service

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# create a file handler
handler = logging.FileHandler('/home/pi/Documents/beacon/try_bluez_log.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)

url = 'http://192.168.225.26:9002/api/v1/tracking/store/data'
url_health = 'http://192.168.225.31:9002/api/v1/health/check'

headers = {"Content-Type": "application/json"}

class Beacon(object):
    
    def __init__(self, data, address):
        self._uuid = data[0]
        self._major = data[1]
        self._minor = data[2]
        self._power = data[3]
        self._rssi = data[4]
        self._address = address
        
    def __str__(self):
        ret = "Beacon: address:{ADDR}\nuuid:{UUID} \nmajor:{MAJOR} \n"\
                "minor:{MINOR} \ntxpower:{POWER} \nrssi:{RSSI}"\
                .format(ADDR=self._address, UUID=self._uuid, MAJOR=self._major,
                        MINOR=self._minor, POWER=self._power, RSSI=self._rssi)
        return ret

service = BeaconService()
beacon_list = []
json_list = []

logger.info("System initialized successfully..")

def get_mac_id(interface='eth0'):
    # Return the MAC address of the specified interface
    try:
        str = open('/sys/class/net/%s/address' %interface).read()
    except:
        str = "00:00:00:00:00:00"
    return str[0:17]

handleException = True
    
def scan_device():
	devices = {}
	is_scan = True
	#if scan_time!=3:
	try:
		devices = service.scan(3)
		print("print(devices)")
		print(devices)
		is_scan = True
	except Exception as e:
		print("scan_device failed : " + str(e))
		is_scan = False
		print("handleException")
		print(handleException)
		
	return devices, is_scan
	
def store_data():
	mac_id = get_mac_id()
	temp_beacon_list = []
	i=0
	devices, is_scan = scan_device()
	
	print(is_scan)	
	if is_scan:
	
		json_data = {}
		json_list = []
		for address, data in list(devices.items()):
			b = Beacon(data, address)
			temp_beacon_list.append(data[0])

			logger.info("==================================")
			#now = datetime.datetime.now()
			#current_time = now.strftime('%Y-%m-%d %H:%M:%S')
			
		now = datetime.datetime.now()
		#current_time = now.strftime('%Y-%m-%d %H:%M:%S')
		current_time = int(round(time.time() * 1000))
		
		print(current_time)
		
		logger.info(current_time)
		logger.info(temp_beacon_list)
		logger.info(len(temp_beacon_list))
		
		print("handleException in store_data")
		print(handleException)
		
		if len(temp_beacon_list) > 0:
			for value in temp_beacon_list:
				if value not in beacon_list:
					print("In",value)
					logger.info(" IN : %s "%value)
					#Update value of json_data dictionary
					
					#json_data = {"status":"IN", "time":current_time, "beacon_id":value, "mac_id":mac_id}
					print("db service called for IN")
					db_service.insert_data("IN", value, current_time)
					
					#Append values to the global list
					beacon_list.append(value)
					json_list.append(json_data)
					
			for val in beacon_list:
				if val not in temp_beacon_list:
					print("Out",val)
					logger.info(" Out : %s "%val)
					#Update value of json_data dictonary
					#json_data = {"status":"OUT", "time":current_time, "beacon_id":val, "mac_id":mac_id}				
					print("db service called for OUT")
					db_service.insert_data("OUT", val, current_time)
					#Append values to the global list
					json_list.append(json_data)
					#Remove value from the globally declared beacon_list
					print("Remove 2nd for")
					beacon_list.remove(val)

		else:
			for values in beacon_list:
				print(values,"OUT")
				logger.info(" Out : %s "%values)
				#Update value of json_data dictonary
				#json_data = {"status":"OUT", "time":current_time, "beacon_id":values, "mac_id":mac_id}
				print("db service called for OUT outer else")
				db_service.insert_data("OUT", values, current_time)
				#Append values to the global list
				json_list.append(json_data)
				#Remove value from the globally declared beacon_list
				beacon_list.remove(values)
        
        #print(json_list)
        #print(beacon_list)

try:
	
	con = lite.connect(db_service.sqlite_file)
	
	with con:

		cur = con.cursor()
		#cur.execute("CREATE TABLE beacon_scanner(id INTEGER PRIMARY KEY, status TEXT, beacon_id TEXT, time TEXT);")
		cur.execute("CREATE TABLE IF NOT EXISTS beacon_scanner(id INTEGER PRIMARY KEY, status TEXT, beacon_id TEXT, time INTEGER);")

except lite.Error, e:

	if con:
		con.rollback()

	print "Error {}:".format(e.args[0])
	sys.exit(1)

finally:

	if con:
		con.close()
		
print("Done.")


def call_data_store():
	print("In beacon scanner")
	logs = db_service.get_actvitylogs_data()
	mac_id = get_mac_id()
	#print(logs)
	#print(len(logs))
	
	#print(json.dumps({'tracking_logs':logs,'mac_id':mac_id}))
	
	activity_logs_data = json.dumps({'tracking_logs':logs,'mac_id':mac_id})
	#print("Formatted data")
	#print(activity_logs_data)
	
	if len(logs) != 0:
		requests.post(url, headers=headers, data=activity_logs_data)

def delete_activity_data():
	print("In method delete_activity_data")
	db_service.delete_activitylogs_data()
	
def call_health_check():
	print("In call health check")
	mac_id = get_mac_id()
	#print(json.dumps({'mac_id':mac_id}))
	requests.post(url_health, headers=headers, data=json.dumps({'mac_id':mac_id}))

schedule.every(5).seconds.do(store_data)
schedule.every(2).minutes.do(call_data_store)
#schedule.every(15).minutes.do(delete_activity_data)
#schedule.every(3).seconds.do(call_health_check)

while 1:
	schedule.run_pending()
	time.sleep(1) 
