import sqlite3 as lite
import sys
import json
import datetime
import time as time_obj

sqlite_file = '/home/pi/Documents/beacon/beacon_scanner/beacon_scanner.sqlite3'    # name of the sqlite database file

def insert_data(status, beacon_id, time):
	
	print("In insert method")
	#Connect to a database file	
	con = lite.connect(sqlite_file)
	now = datetime.datetime.now()
	current_time = now.strftime('%Y-%m-%d %H:%M:%S')
	
	activity_log_start_datetime = datetime.datetime.now() - datetime.timedelta(minutes=15)
	
	subtracted_date_time = time_obj.mktime(activity_log_start_datetime.timetuple()) + activity_log_start_datetime.microsecond / 1E6
	
	activity_log_start_timestamp = int(round(subtracted_date_time * 1000))
	activity_log_end_timestamp = int(round(time_obj.time() * 1000))
	
	# Perform ops
	with con:

		cur = con.cursor()
		#SQLite query to insert a record in db file
		cur.execute("INSERT INTO beacon_scanner (status, beacon_id, time) VALUES (?, ?, ?)",(status, beacon_id, time))
		
		#print("-----------------------")
		
		#cur.execute("SELECT id, status, beacon_id, strftime('%Y-%m-%dT%H:%M:%S',datetime(time/1000, 'unixepoch')) FROM beacon_scanner")
		
		#rows = cur.fetchall()

		#for row in rows:
		#	print "row"
		#	print row
		#print("-----------------------")
			
def get_actvitylogs_data():
	print("In get activity logs data method")
	connection = lite.connect(sqlite_file)
	cursor = connection.cursor()
	
	activity_log_start_datetime = datetime.datetime.now() - datetime.timedelta(minutes=2)
	
	subtracted_date_time = time_obj.mktime(activity_log_start_datetime.timetuple()) + activity_log_start_datetime.microsecond / 1E6
	
	activity_log_start_timestamp = int(round(subtracted_date_time * 1000))
	activity_log_end_timestamp = int(round(time_obj.time() * 1000))
	
	result = cursor.execute("SELECT status, beacon_id, strftime('%Y-%m-%dT%H:%M:%S',datetime(time/1000, 'unixepoch')) FROM beacon_scanner WHERE time > ? and time < ?",(activity_log_start_timestamp, activity_log_end_timestamp))
	
	logs = []
	
	for row in result:
		logs.append({'beacon_id':row[1], 'status':row[0], 'time':row[2]})
		
	return logs;
	
def delete_activitylogs_data():
	print("In delete activity logs data method")
	con = lite.connect(sqlite_file)
	
	activity_log_start_datetime = datetime.datetime.now() - datetime.timedelta(minutes=15)
	#print(activity_log_start_datetime)
	
	subtracted_date_time = time_obj.mktime(activity_log_start_datetime.timetuple()) + activity_log_start_datetime.microsecond / 1E6
	
	activity_log_start_timestamp = int(round(subtracted_date_time * 1000))
	#print(activity_log_start_timestamp)
	
	with con:
		
		cur = con.cursor()
	
		cur.execute("DELETE FROM beacon_scanner WHERE time < ?",(activity_log_start_timestamp,))
